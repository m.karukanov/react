import React from "react";
import './style.scss';

const Header = () => (
  <header className="header">
    <div className="container header__wrapper">
      <a href="/" className="header__logo">
        С  М
        <span>супер магазин</span>
      </a>
      <a href="/cart" className="header__cart">
        Корзина
        <span>(0)</span>
      </a>
    </div>
  </header>
);

export default Header;
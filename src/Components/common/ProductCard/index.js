import React from "react";
import './style.scss';

const ProductCard = (props) => (
  <div className="product-card">
    <a className="product-card__link" href="/">
      <span className="product-card__img">
        <img src={props.img} alt="" title="" />
      </span>
      <span className="product-card__name">{props.name}</span>
      <span className="product-card__sum">{props.sum} ₽</span>
    </a>
    <button className="product-card__button">Купить</button>
  </div>
);

export default ProductCard;
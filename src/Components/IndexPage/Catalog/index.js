import React from "react";
import './style.scss';
import ProductCard from '../../common/ProductCard'

const productList = [
  {
    img: '/product-1.png',
    name: 'Наушники Bowers & Wilkins PX золотые',
    sum: '25 990',
  },
  {
    img: '/product-2.png',
    name: 'Фитнес браслет HerzBand Next с измерением давления',
    sum: '2 490',
  },
  {
    img: '/product-3.png',
    name: 'Бесконтактный медицинский термометр Sensitec NF-3101',
    sum: '7 500',
  },
  {
    img: '/product-4.png',
    name: 'Лонгборд Ridex Traveller 40.15″X9.7″, ABEC-9',
    sum: '8 250',
  },
];

const Catalog = () => (
  <div className="catalog">
    <h2>Каталог товаров</h2>
    <ul>
      {productList.map((item) => {
        return (
          <li key={item.name}>
            <ProductCard
              name={item.name}
              sum={item.sum}
              img={item.img}
            />
          </li>
        );
      })}
    </ul>
    <a href="/" className="catalog__link">Посмотреть все товары</a>
  </div>
);

export default Catalog;
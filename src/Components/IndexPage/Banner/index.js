import React from "react";
import './style.scss';

const Banner = () => (
  <div className="banner">
    <img src="/banner.png" alt="" title="" />
  </div>
);

export default Banner;
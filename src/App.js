import React from 'react';
import Header from './Components/common/Header';
import Footer from './Components/common/Footer';

import Banner from './Components/IndexPage/Banner';
import Advantage from './Components/IndexPage/Advantage';
import Catalog from './Components/IndexPage/Catalog';

function App() {
  return (
    <div className="App">
      <Header />
      <div className="container">
        <Banner />
        <Catalog />
        <Advantage />
      </div>
      <Footer />
    </div>
  );
}

export default App;
